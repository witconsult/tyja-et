<?php
/**
 * Child theme functions
 *
 * Functions file for child theme, enqueues parent and child stylesheets by default.
 *
 * @since	1.0.0
 * @package aa
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// appends a seo and wit-customer page menu item to the primary menu
if ( is_user_logged_in() ) {
		add_filter( 'wp_nav_menu_items', 'add_customerlink_to_menu', 10, 2 );
}

function add_customerlink_to_menu ( $items, $args ) {

    if ($args->theme_location == 'primary-menu') {
			if (get_page_by_path( 'aedar' )) {
        $items = $items . '<li><a href="' . get_site_url() . '/aedar" class="wepi_menu_customer_link">æ</a></li>';
			}
			// TODO: phase out and remove at some point
			if (get_page_by_path( 'wit' )) {
        $items = $items . '<li><a href="' . get_site_url() . '/wit" class="wepi_menu_customer_link">wit</a></li>';
			}
			// TODO: phase out and remove at some point
			if (get_page_by_path( 'seo' )) {
				$items = $items . '<li><a href="' . get_site_url() . '/seo" class="wepi_menu_customer_link">SEO</a></li>';
			}
    }
    return $items;
}



if ( ! function_exists( 'aa_enqueue_styles' ) ) {
	// Add enqueue function to the desired action.
	add_action( 'wp_enqueue_scripts', 'aa_enqueue_styles' );

	/**
	 * Enqueue Styles.
	 *
	 * Enqueue parent style and child styles where parent are the dependency
	 * for child styles so that parent styles always get enqueued first.
	 *
	 * @since 1.0.0
	 */
	function aa_enqueue_styles() {
		// Parent style variable.
		$parent_style = 'parent-style';

		// Enqueue Parent theme's stylesheet.
		//wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );

		// Enqueue Child theme's stylesheet.
		// Setting 'parent-style' as a dependency will ensure that the child theme stylesheet loads after it.
		wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( $parent_style ) );
	}
}


/*
*	hide the Divi "Project" post type.

add_filter( 'et_project_posttype_args', 'mytheme_et_project_posttype_args', 10, 1 );
function mytheme_et_project_posttype_args( $args ) {
	return array_merge( $args, array(
		'public'              => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => false,
		'show_in_nav_menus'   => false,
		'show_ui'             => false
	));
}
*/

/**
 *	unregister the Divi "Project" post type.
 */
 if ( ! function_exists( 'et_pb_register_posttypes' ) ) :
	function et_pb_register_posttypes() {
		global $wp_post_types;
		if ( isset( $wp_post_types[ $post_type ] ) ) {
			unset( $wp_post_types[ $post_type ] );
			return true;
		}
		return false;
	}
endif;

// load translations
function tyja_load_translations() {
	load_child_theme_textdomain( 'Divi', get_stylesheet_directory() . '/languages' );
	load_child_theme_textdomain( 'et_builder', get_stylesheet_directory() . '/languages/builder' );
}
add_action( 'after_setup_theme', 'tyja_load_translations' );



/* register shortcodes */
include dirname(__FILE__).'/shortcodes/shortcodes.php';
add_action( 'init', 'register_shortcodes');
